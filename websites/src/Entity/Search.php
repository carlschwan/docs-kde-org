<?php

namespace App\Entity;

class Search
{
    protected $searchTerm;

    /**
     * @return string|null
     */
    public function getSearchTerm(): ?string
    {
        return $this->searchTerm;
    }

    /**
     * @param string|null $search
     */
    public function setSearchTerm(?string $search): void
    {
        $this->searchTerm = $search;
    }
}

