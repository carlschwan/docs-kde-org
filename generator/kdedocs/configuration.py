#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2015  Luigi Toscano <luigi.toscano@tiscali.it>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) version 3, or any
# later version accepted by the membership of KDE e.V. (or its
# successor approved by the membership of KDE e.V.), which shall
# act as a proxy defined in Section 6 of version 3 of the license.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.  If not, see <http://www.gnu.org/licenses/>.

import codecs
import configparser
import logging
import os
import sys


LOGGER = logging.getLogger(__name__)


class InvalidBranchException(Exception):
    pass


class DocConfiguration(object):

    BASEDIR = os.path.dirname(os.path.realpath(__file__))
    BRANCH_CONF_PREFIX = 'branch.'
    ENV_CONF_PREFIX = 'env.'

    def __init__(self, conffile):
        self._conf = configparser.ConfigParser()

        with codecs.open(conffile, 'r', encoding='utf-8') as cf:
            self._conf.readfp(cf)

        self._website_dir = None
        self._work_dir = None
        self._all_languages = None
        self._all_packages = None
        self._all_branches = None
        self._static_files = None
        self._enabled_branches = None
        self._configured_envs = None
        self._resource_dir = None

    @property
    def enabled_branches(self):
        if self._enabled_branches is None:
            # branches requested explicitly
            try:
                requested_branches = self._conf.get('common', 'branches')
            except (configparser.NoSectionError,
                    configparser.NoOptionError) as e:
                requested_branches = None

            # branches with a configuration section
            configured_branches = [br_sec[len(self.BRANCH_CONF_PREFIX):]
                                   for br_sec in self._conf.sections()
                                   if br_sec.startswith(
                                       self.BRANCH_CONF_PREFIX)]

            if requested_branches is None:
                # no requested branches? Then all configured branches are
                # available
                self._enabled_branches = configured_branches
            else:
                self._enabled_branches = [branch.strip() for branch in
                                          requested_branches.split(',')
                                          if branch in configured_branches]
        return self._enabled_branches

    @property
    def all_packages(self):
        if self._all_packages is None:
            self._all_packages = []
            try:
                with open('packages', 'r') as pfile:
                    self._all_packages = [pkg.strip() for pkg in pfile]
            except:
                pass
        return self._all_packages

    @property
    def all_languages(self):
        if self._all_languages is None:
            self._all_languages = []
            try:
                with open('languages', 'r') as lfile:
                    self._all_languages = [lang.strip() for lang in lfile]
            except:
                pass
            # always add 'en' as first element
            self._all_languages.insert(0, 'en')
        return self._all_languages

    @property
    def configured_packages(self):
        try:
            pkgs = [pkg.strip() for pkg in
                    self._conf.get('common', 'packages').split(',')]
        except (configparser.NoSectionError, configparser.NoOptionError) as e:
            pkgs = self.all_packages
        return pkgs

    @property
    def configured_languages(self):
        try:
            langs = [lang.strip() for lang in
                     self._conf.get('common', 'languages').split(',')]
        except (configparser.NoSectionError, configparser.NoOptionError) as e:
            langs = self.all_languages
        return langs

    @property
    def configured_environments(self):
        if self._configured_envs is None:
            # branches with a configuration section
            self._configured_envs = [env_sec[len(self.ENV_CONF_PREFIX):]
                                     for env_sec in self._conf.sections()
                                     if env_sec.startswith(
                                        self.ENV_CONF_PREFIX)]
        return self._configured_envs

    @property
    def static_files(self):
        if self._static_files is None:
            value_static_files = self.get_value('static_files')
            if value_static_files:
                self._static_files = [sf.strip() for sf in
                                      value_static_files.split(',')]
            else:
                self._static_files = []
        return self._static_files

    def _get_section_value(self, section, key):
        try:
            value = self._conf.get(section, key)
        except (configparser.NoSectionError, configparser.NoOptionError) as e:
            value = None
        return value

    def get_branch_value(self, branch, key):
        """Get the value of a specific key for a certain branch."""
        if branch is None or key is None:
            return None
        return self._get_section_value(self.BRANCH_CONF_PREFIX + branch, key)

    def get_env_value(self, env, key):
        """Get the value of a specific key for a certain environment."""
        if env is None or key is None:
            return None
        return self._get_section_value(self.ENV_CONF_PREFIX + env, key)

    def get_value(self, key):
        """Get the value of a specific key for the common configuration."""
        if key is None:
            return None
        return self._get_section_value('common', key)

    @property
    def websitedir(self):
        """Directory of the generated website where the documentation files
        (original and localized) are fetched.
        If old is specified, the old directory is used.
        """
        if self._website_dir is None:
            self._website_dir = self.get_value('website_dir')
            if self._website_dir is None:
                self._website_dir = os.path.join(self.BASEDIR, 'website')
        return self._website_dir

    @property
    def nextwebsitedir(self):
        """Previous website."""
        return self.websitedir + '.next'

    @property
    def workdir(self):
        """Work directory."""
        if self._work_dir is None:
            self._work_dir = self.get_value('work_dir')
            if self._work_dir is None:
                self._work_dir = os.path.join(self.BASEDIR, 'work')
        return self._work_dir

    @property
    def resource_dir(self):
        """Directory for resources (scripts, php files)."""
        if self._resource_dir is None:
            self._resource_dir = os.path.dirname(os.path.abspath(sys.argv[0]))
        return self._resource_dir

    def website_branch_dir(self, branch, next=False):
        """Operation directory for the website."""
        if branch not in self.enabled_branches:
            raise InvalidBranchException('%s is not a configured branch' %
                                         (branch))
        if not next:
            return os.path.join(self.websitedir, branch)
        else:
            return os.path.join(self.nextwebsitedir, branch)

    def website_lang_dir(self, branch, lang, next=False):
        return os.path.join(self.website_branch_dir(branch, next), lang)

    def website_pkg_dir(self, branch, lang, pkg, next=False):
        return os.path.join(self.website_lang_dir(branch, lang, next), pkg)

    def op_branch_dir(self, branch):
        """Operation directory with scratch data (repositories, build
        directories, etc).
        """
        if branch not in self.enabled_branches:
            raise InvalidBranchException('%s is not a configured branch' %
                                         (branch))
        return os.path.join(self.workdir, branch)

    def op_lang_dir(self, branch, lang):
        return os.path.join(self.op_branch_dir(branch), lang)

    def op_pkg_dir(self, branch, lang, pkg):
        return os.path.join(self.op_lang_dir(branch, lang), pkg)

    def get_svn_dir(self, branch):
        """svn-specific directory for the requested branch."""
        if branch not in self.enabled_branches:
            raise InvalidBranchException('%s is not a configured branch' %
                                         (branch))
        try:
            svn_branch = self._conf.get(self.BRANCH_CONF_PREFIX + branch,
                                        'l10n_path')
        except (configparser.NoOptionError) as e:
            svn_branch = branch
        return svn_branch

    def work_dir_walker(self, branches=None, languages=None, packages=None):
        """Walks through the work directory (branches, languages, packages)
        """
        if not languages:
            languages = self.configured_languages
        if not branches:
            branches = self.enabled_branches
        if not packages:
            packages = self.configured_packages
        LOGGER.debug('%s, %s, %s' % (branches, languages, packages))

        for branch in branches:
            for lang in languages:
                for pkg in packages:
                    LOGGER.debug('%s, %s, %s' % (branch, lang, pkg))
                    curr_path = self.op_pkg_dir(branch, lang, pkg)
                    yield (branch, lang, pkg, curr_path)
