#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2015  Luigi Toscano <luigi.toscano@tiscali.it>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) version 3, or any
# later version accepted by the membership of KDE e.V. (or its
# successor approved by the membership of KDE e.V.), which shall
# act as a proxy defined in Section 6 of version 3 of the license.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
import os
import logging
import shutil
import subprocess

import pysvn

from .utils import mkdirp


LOGGER = logging.getLogger(__name__)


class RetrieveDocumentationBranch(object):

    def __init__(self, branch, config, env_manager):
        self.config = config
        self.svn_client = pysvn.Client()
        self.branch = branch
        self.env = env_manager.get_env(branch)
        # TODO: this path could be configurable
        self.base_svn_url = 'svn://anonsvn.kde.org/home/kde'

        self.svn_branch = self.config.get_svn_dir(branch)
        self.op_branch_dir = self.config.op_branch_dir(branch)
        mkdirp(self.op_branch_dir)
        self.script_path = os.path.join(self.op_branch_dir, 'scripts')

    def checkout_scripts(self):
        try:
            self.svn_client.checkout('%s/%s/scripts/' % (self.base_svn_url,
                                                         self.svn_branch),
                                     self.script_path, depth=pysvn.depth.empty)
            for checked_file in ['documentation_paths',
                                 'populate_documentation.sh']:
                self.svn_client.update(os.path.join(self.script_path,
                                                    checked_file),
                                       depth=pysvn.depth.infinity)
        except pysvn.ClientError as e:
            # FIXME: fail?
            LOGGER.error(str(e))

    def _checkout_doc_en(self):
        documentation_dir = os.path.join(self.op_branch_dir, 'documentation')
        doc_en_dir = os.path.join(self.op_branch_dir, 'en')
        # populate_documentation.sh works on a 'documentation' directory
        if os.path.isdir(doc_en_dir):
            if os.path.isdir(documentation_dir):
                # leftover? Remove it
                shutil.rmtree(documentation_dir)
            shutil.rmtree(doc_en_dir)
        for package in self.config.configured_packages:
            LOGGER.debug('%s, "en": getting documentation for package "%s"' %
                         (self.branch, package))

            proc = subprocess.Popen([os.path.join(self.script_path,
                                                  'populate_documentation.sh'),
                                     package], cwd=self.op_branch_dir,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
            proc_out, proc_err = proc.communicate()
            proc_rc = proc.returncode
            if proc_rc != 0:
                LOGGER.debug('%s\n%s' % (proc_out, proc_err))

        # TODO: maybe use a symlink?
        os.rename(documentation_dir, doc_en_dir)

    def checkout_doc(self):
        self.env.checkout_doc_init(self.branch)
        self.checkout_scripts()
        for lang in self.config.configured_languages:
            try:
                lang_dir = os.path.join(self.op_branch_dir, lang)
                if lang == 'en':
                    LOGGER.info('%s, "en": getting documentation' %
                                (self.branch))
                    self._checkout_doc_en()
                elif not os.path.isdir(lang_dir):
                    LOGGER.info('%s, "%s": checkout of documentation' %
                                (self.branch, lang))
                    self.svn_client.checkout('%s/%s/%s/docs' %
                                             (self.base_svn_url,
                                              self.svn_branch, lang),
                                             lang_dir,
                                             depth=pysvn.depth.infinity)
                else:
                    LOGGER.info('%s, %s: update of documentation' %
                                (self.branch, lang))
                    self.svn_client.update(lang_dir,
                                           depth=pysvn.depth.infinity)
            except Exception as e:
                LOGGER.warning('Error while checking out documentation for '
                               '"%s": %s' % (lang, e))
        self.env.checkout_doc_post(self.branch,
                                   self.config.configured_languages)
