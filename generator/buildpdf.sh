#!/bin/bash

# for debug uncomment, go to the folder and run this script with parameter index.docbook
#export SGML_CATALOG_FILES=/home/docs/docs/development/kdoctools/customization/catalog:/home/docs/docs/development/kdoctools/customization/catalog.xml:/home/docs/docs/development/kdoctools/docbook/xml-dtd-4.2/docbook.cat
export PDF_FILE_NAME=$(pwd | awk -F/ '{ print $NF }')

DBLATEX_BASE_DIR=${DBLATEX_BASE_DIR:-/home/docs/docs}

# add -d to command below to keep the /tmp folder, so you can examine the generated tex.
${DBLATEX_BASE_DIR}/dblatex-cvs-install/bin/dblatex -b pdftex --style \
	kdestyle\
	-o $PDF_FILE_NAME.pdf \
	-P latex.output.revhistory=0  -P newtbl.use=1 \
	-P imagedata.default.scale=pagebound \
	-P literal.width.ignore=1 \
	-I $KDEDIR/share/doc/HTML/en/ \
	-X \
        $1
