<sect1>
   <title>
   Publishing with dblatex
  </title>
  <para>
   To publish your document, you just need to use the script <filename>dblatex</filename>. 
  </para>
   <sect2>
    <title>
    Synopsis
   </title>
   <programlisting>
<![CDATA[dblatex [-t {tex|dvi|ps|pdf}] [-b {dvips|pdftex}] [-o output] [other options] file.{xml|sgml}
]]>   </programlisting>
   </sect2>
   <sect2>
    <title>
    Description
   </title>
   <para>
    The script works on an XML or SGML file and can produce LaTeX, DVI, Postscript and PDF output. By default (i.e. without option) a PDF file is produced in the same directory where the input file is, with the same base name.
   </para>
   </sect2>
   <sect2>
    <title>
    Options
   </title>
   <variablelist>
    <varlistentry>
    <term>
<option>-t <replaceable>format</replaceable></option>
</term><listitem><para>Output format. By default the format is PDF.
    </para>
   </listitem>
   </varlistentry>
    <varlistentry>
    <term>
<option>-b <replaceable>driver</replaceable></option>
</term><listitem><para>Backend driver to use. The available drivers are &ldquo;dvips&rdquo; (latex) and &ldquo;pdftex&rdquo; (pdflatex). By default the &ldquo;dvips&rdquo; driver is selected. See also <xref linkend="sec-process">.
    </para>
   </listitem>
   </varlistentry>
    <varlistentry>
    <term>
<option>-f <replaceable>fig_format</replaceable></option>
</term><listitem><para>Input figures format, specified to have on the fly conversion. See also <xref linkend="sec-figconv">.
    </para>
   </listitem>
   </varlistentry>
    <varlistentry>
    <term>
<option>-I <replaceable>fig_path</replaceable></option>
</term><listitem><para>Additional lookup paths of the figures. See <xref linkend="sec-lookup">.
    </para>
   </listitem>
   </varlistentry>
    <varlistentry>
    <term>
<option>-o <replaceable>output</replaceable></option>
</term><listitem><para>Output filename. When not used, the input file name is used, with a suffix related to the output format.
    </para>
   </listitem>
   </varlistentry>
    <varlistentry>
    <term>
<option>-d</option>
</term><listitem><para>Debug mode. It only keeps the temporary directory in which dblatex actually works. <xref linkend="sec-debug"> explains how you can use it.
    </para>
   </listitem>
   </varlistentry>
    <varlistentry>
    <term>
<option>-p <replaceable>config_file</replaceable></option>
</term><listitem><para>Specify a configuration file. See <xref linkend="sec-param">.
    </para>
   </listitem>
   </varlistentry>
    <varlistentry>
    <term>
<option>-P <replaceable>param=value</replaceable></option>
</term><listitem><para>Set an XSL parameter value from command line.
See <xref linkend="sec-param-value">.
    </para>
   </listitem>
   </varlistentry>
    <varlistentry>
    <term>
<option>-S <replaceable>spec_file</replaceable></option>
</term><listitem><para>Specification file. A specification file can be used to group all the options and customizations to apply. See <xref linkend="sec-specs">.
    </para>
   </listitem>
   </varlistentry>
   <varlistentry>
    <term>
<option>-T <replaceable>style</replaceable></option>
</term><listitem><para>Rendering style to use. Several rendering style (also called
LaTeX style) are provided by default. See <xref linkend="sec-style">.
    </para>
   </listitem>
   </varlistentry>
    <varlistentry>
    <term>
<option>-x <replaceable>xslt_options</replaceable></option>
</term><listitem><para>Options to pass to the XSLT. The example below passes the options &ldquo;--timing&rdquo; and &ldquo;--profile&rdquo; to the XSLT. Using this option supposes that you know the supported XSLT options. 
    </para>
    <programlisting>
<![CDATA[dblatex -x "--timing --profile" file.sgml]]></programlisting>
   </listitem>
   </varlistentry>
    <varlistentry>
    <term>
<option>-X</option>
</term><listitem><para>Disable the external text file support. This support is
needed for callouts on external files referenced by <sgmltag>textdata</sgmltag>
or <sgmltag>imagedata</sgmltag>, but it can be disabled if the document does not
contain such callouts. Disabling
this support can improve the processing performance for big
documents.
    </para>
   </listitem>
   </varlistentry>
   </variablelist>
   </sect2>
   <sect2>
    <title>
    Other options
   </title>
   <para>
    Other options are defined to customize the tool, but they should be used in exceptional cases, for test or debug purpose. These options are defined in section <xref linkend="sec-specs"> and should normaly be set through a specification file.
   </para>
   </sect2>
  </sect1>

