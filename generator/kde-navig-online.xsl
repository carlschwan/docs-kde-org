<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

  <!-- only for the online version -->
  <!-- obfuscate email addresses -->
  <xsl:template match="email">
    <xsl:call-template name="inline.monoseq">
      <xsl:with-param name="content">
        <xsl:text>(</xsl:text>
        <xsl:call-template name="replaceCharsInString">
          <xsl:with-param name="stringIn" select="."/>
          <xsl:with-param name="charsIn" select="'@'"/>
          <xsl:with-param name="charsOut" select="'  '"/>
        </xsl:call-template>
        <xsl:text>)</xsl:text>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>
  <xsl:template name="replaceCharsInString">
    <xsl:param name="stringIn"/>
    <xsl:param name="charsIn"/>
    <xsl:param name="charsOut"/>
    <xsl:choose>
      <xsl:when test="contains($stringIn,$charsIn)">
        <xsl:value-of select="concat(substring-before($stringIn,$charsIn),$charsOut)"/>
        <xsl:call-template name="replaceCharsInString">
          <xsl:with-param name="stringIn" select="substring-after($stringIn,$charsIn)"/>
          <xsl:with-param name="charsIn" select="$charsIn"/>
          <xsl:with-param name="charsOut" select="$charsOut"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$stringIn"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <!-- end "only for the online version" -->

  <xsl:template name="header.navigation">
    <xsl:param name="prev" select="/foo"/>
    <xsl:param name="next" select="/foo"/>
    <xsl:variable name="home" select="/*[1]"/>
    <xsl:variable name="up" select="parent::*"/>
    <xsl:if test="$suppress.navigation = '0'">
      <header class="header clearfix">
        <nav id="kHeaderNav" class="navbar navbar-expand-sm container-fluid">
          <a class="navbar-brand navbar-brand-active" href="/" id="KDEGlobalLogo">
            <img src="/images/icon-kde.svg" width="40px" height="40px"/>
            <span>KDE Documentation</span>
          </a>
          <xsl:apply-templates select="." mode="title.markup"/>
        </nav>
      </header>

      <!-- output navigation links -->
      <nav aria-label="Documentation navigation" class="mt-2">
        <ul class="pagination d-flex justify-content-center">
          <!-- Previous -->
          <xsl:if test="count($prev)>0">
            <li class="page-item">
              <a accesskey="p" class="page-link">
                <xsl:attribute name="href">
                  <xsl:call-template name="href.target">
                    <xsl:with-param name="object" select="$prev"/>
                  </xsl:call-template>
                </xsl:attribute>
                <xsl:call-template name="gentext.nav.prev"/>
              </a>
            </li>
          </xsl:if>
          <xsl:if test="count($prev)=0">
            <li class="page-item disabled">
              <span class="page-link">
                <xsl:call-template name="gentext.nav.prev"/>
              </span>
            </li>
          </xsl:if>

          <!-- page title -->
          <li class="page-item">
            <span class="page-link">
              <a accesskey="h">
                <xsl:attribute name="href">
                  <xsl:call-template name="href.target">
                    <xsl:with-param name="object" select="$up"/>
                  </xsl:call-template>
                </xsl:attribute>
                <xsl:apply-templates select="." mode="title.markup"/>
              </a>
            </span>
          </li>

          <!-- Next -->
          <xsl:if test="count($next)>0">
            <li class="page-item">
              <a accesskey="p" class="page-link">
                <xsl:attribute name="href">
                  <xsl:call-template name="href.target">
                    <xsl:with-param name="object" select="$next"/>
                  </xsl:call-template>
                </xsl:attribute>
                <xsl:call-template name="gentext.nav.next"/>
              </a>
            </li>
          </xsl:if>
          <xsl:if test="count($next)=0">
            <li class="page-item disabled">
              <span class="page-link">
                <xsl:call-template name="gentext.nav.next"/>
              </span>
            </li>
          </xsl:if>
        </ul>
      </nav>
    </xsl:if>
  </xsl:template>

  <!-- ==================================================================== -->

  <xsl:template name="footer.navigation">
    <xsl:param name="prev" select="/foo"/>
    <xsl:param name="next" select="/foo"/>
    <xsl:variable name="home" select="/*[1]"/>
    <xsl:variable name="up" select="parent::*"/>

    <xsl:if test="$suppress.navigation = '0'">
      <!-- output navigation links -->
      <nav aria-label="Documentation navigation" class="mt-2">
        <ul class="pagination d-flex justify-content-center">
          <!-- Previous -->
          <xsl:if test="count($prev)>0">
            <li class="page-item">
              <a accesskey="p" class="page-link">
                <xsl:attribute name="href">
                  <xsl:call-template name="href.target">
                    <xsl:with-param name="object" select="$prev"/>
                  </xsl:call-template>
                </xsl:attribute>
                <xsl:call-template name="gentext.nav.prev"/>
              </a>
            </li>
          </xsl:if>
          <xsl:if test="count($prev)=0">
            <li class="page-item disabled">
              <span class="page-link">
                <xsl:call-template name="gentext.nav.prev"/>
              </span>
            </li>
          </xsl:if>

          <!-- TOC -->
          <li class="page-item">
            <span class="page-link">
              <a accesskey="h">
                <xsl:attribute name="href">
                  <xsl:call-template name="href.target">
                    <xsl:with-param name="object" select="$up"/>
                  </xsl:call-template>
                </xsl:attribute>
                <xsl:call-template name="gentext.nav.home"/>
              </a>
            </span>
          </li>

          <!-- Next -->
          <xsl:if test="count($next)>0">
            <li class="page-item">
              <a accesskey="p" class="page-link">
                <xsl:attribute name="href">
                  <xsl:call-template name="href.target">
                    <xsl:with-param name="object" select="$next"/>
                  </xsl:call-template>
                </xsl:attribute>
                <xsl:call-template name="gentext.nav.next"/>
              </a>
            </li>
          </xsl:if>
          <xsl:if test="count($next)=0">
            <li class="page-item disabled">
              <span class="page-link">
                <xsl:call-template name="gentext.nav.next"/>
              </span>
            </li>
          </xsl:if>
        </ul>
      </nav>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
